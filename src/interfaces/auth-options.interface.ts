export interface IAuthService {
  name: string;
  data: any;
  expiresOn: number;
}
export interface IAuth {
    service: IAuthService;
    user: any;
}