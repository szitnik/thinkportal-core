export interface IOptions {
    title: string;
    layout: ILayout;
    auth: IAuthConfig;
    defaultRoute: string;
    loginRoute: string;
    settings: any;
    routes: IRouteDictionary;
}

export interface ILayout {
    package: string;
    theme?: string;
    name?: string;
    views?: any;
    reference?: {};
}

export interface IComponentParams {
    components: IComponentDictionary;
    core: any;
    data: any;
    name: string;
    class?: string;
    style?: string;
}

export interface ILayoutComponent {
    reference: IDataView;
    params: IComponentParams;
    package: string;
}

export interface IRouteDictionary {
    [index: string]: IRoute;
}

export interface IRoute {
    extends?: string;
    url?: string;
    name?: string;
    icon?: string;
    title?: string;
    properties?: {};
    auth?: IAuthRouteConfig;
    view?: IViewConfig;
    removeComponents?: string[];
    components?: IComponent[];
}

export interface ILocationRoute {
    name: string;
    params: string[];
}

export interface IComponentDictionary {
    [index: string]: IComponent[];
}

export interface IComponent {
    id: string;
    uniqueId?: number;
    name: string;
    package: string;
    reference?: any;
    container: string;
    params: any;
    modifiers?: IModifiers;
    beforeComponent?: string;
    afterComponent?: string;
}

export interface IModifiers {
    class?: string;
    style?: string;
}

export interface IDataView {
    date: string;
    options?: IRoute,
    view?: any,
    name?: string,
    components?: IComponentDictionary
}

export interface IAuthConfig {
    service: string;
    settings: any;
}

export interface IAuthRouteConfig {
    required?: boolean;
}

export interface IViewConfig {
    type?: string;
}