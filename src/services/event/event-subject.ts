import {BehaviorSubject} from 'rxjs/Rx';
import {IEvent} from '../../interfaces/event-options.interface';

export class EventSubject extends BehaviorSubject<IEvent> {

    private eventName: string;

    constructor(eventName: string) {
        super(<IEvent>{name:null});
        this.setEventName(eventName);
    }

    public setEventName(eventName: string): void {
        this.eventName = eventName;
    }

    public getEventName(): string {
        return this.eventName;
    }
}