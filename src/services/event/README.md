# Events

EventService is used for managing events within the portal. Event source can emit events with a call to:
```
EventService.emit(eventName:string, eventData?:any);
```
Events should be namespaced. Event emitter should use its package name and specific event name to construct final event name.
```
Auth.Loggedin
```
Where "Auth" is the name of the package and "Loggedin" is the event name.

Interested parties can subscribe to events. When an event is emitted, the subscribed party is notified by calling the provided callback.
Subscriber can define which events it is interested in:

- one specific event
```
EventService.subscribe("Auth.Loggedin", onNextCallback);
```
- whole class of events (e.g. "Auth")
```
EventService.subscribe("Auth", onNextCallback);
```
- an array of events
```
EventService.subscribe(["Auth.Loggedin", "Auth.Loggedout"], onNextCallback);
```

Subscribe method accepts:

- event name (string or array | required)
- onNext callback (required)
- onError callback (optional)
- onComplete callback (optional)