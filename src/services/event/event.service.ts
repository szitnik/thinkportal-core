import { IEvent } from '../../interfaces/event-options.interface';
import { Subscription } from 'rxjs/Rx';
import { EventSubject } from './event-subject';

export class EventService {

  protected subjects: EventSubject[] = [];

  constructor() { }

  private getEventSubjects(eventNameOrNames: string | string[]): EventSubject[] {
    let eventNames: string[] = [];
    if (typeof eventNameOrNames === 'string') {
      eventNames.push(eventNameOrNames);
    } else {
      eventNames = eventNameOrNames;
    }

    let subjects: EventSubject[] = [];
    for (let eventName of eventNames) {
      let foundSubject: EventSubject;
      for (let subject of this.subjects) {
        if (eventName === subject.getEventName()) {
          foundSubject = subject;
          break;
        }
      }
      if (foundSubject !== undefined) {
        subjects.push(foundSubject);
      } else {
        let subject: EventSubject = new EventSubject(eventName);
        this.subjects.push(subject);
        subjects.push(subject);
      }
    }

    return subjects;
  }

  public subscribe(eventNameOrNames: string | string[], onNext, onError?, onComplete?): Subscription[] {
    let subscriptions: Subscription[] = [];
    this.getEventSubjects(eventNameOrNames).forEach((subject: EventSubject) => {
      subscriptions.push(
        subject
          .filter((event: IEvent) => this.filterEvent(event))
          .map((event: IEvent) => this.mapEvent(event))
          .subscribe(onNext, onError, onComplete));
    });
    return subscriptions;
  }

  public emit(eventName: string, eventData?: any): void {
    this.getEventSubjects(eventName).forEach((subject: EventSubject) => {
      subject.next(<IEvent>{ name: eventName, data: eventData });
    });
  }

  /**
   * need to implement unsubscribe
   */
  public unsubscribe() {

  }

  private mapEvent(currentEvent: IEvent): IEvent {
    return currentEvent;
  }

  private filterEvent(currentEvent: IEvent): boolean {
    // Skip null events
    if (currentEvent.name === null) {
      return false;
    }
    return true;
  }

}
