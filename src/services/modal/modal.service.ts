import {Injectable, TemplateRef} from '@angular/core';
import {Modal, BSModalContext} from 'angular2-modal/plugins/bootstrap';
import {overlayConfigFactory, DialogRef} from "angular2-modal";
import * as _ from 'lodash';

@Injectable()
export class ModalService {

    private modals: any[] = [];
    private showing: boolean = false;

    constructor(private modal: Modal) {}

    // Hack to support final variables (can't be changed)
    public static get ALERT(): string { return 'ALERT'; }
    public static get TEMPLATE(): string { return 'TEMPLATE'; }

    public open(options: any) {
        if (_.isEmpty(options.type)) {
            options.type = ModalService.ALERT;
        }
        this.modals.push(options);

        if (!this.showing) {
            this.createNext();
        }
    }

    private showNext() {
        if (this.modals.length > 0) {
            this.createNext();
        } else {
            this.showing = false;
        }
    }

    private createNext() {
        if (this.modals.length > 0) {
            this.showing = true;
            let nextModalOpts = this.modals.shift();
            let nextModal;
            switch (nextModalOpts.type) {
                case ModalService.ALERT:
                    nextModal = this.createAlert(nextModalOpts.title, nextModalOpts.message);
                    break;

                case ModalService.TEMPLATE:
                    nextModal = this.createTemplate(nextModalOpts.templateRef);
                    break;

                default:
                    this.showNext();
                    return;
            }

            nextModal
                .then((modal: DialogRef<any>) => {
                    if (nextModalOpts.onCreate !== undefined) {
                        nextModalOpts.onCreate(modal);
                    }
                    modal.onDestroy.subscribe(() => {
                        this.showNext();
                    })
                });
        }
    }

    private createAlert(title: string, message: string) {
        return this.modal.alert()
            .showClose(true)
            .title(title)
            .body(`<p>${message}</p>`)
            .open();
    }

    private createTemplate(templateRef: TemplateRef<any>) {
        return this.modal
            .open(
                templateRef,
                overlayConfigFactory({ isBlocking: false }, BSModalContext)
            );
    }

}
