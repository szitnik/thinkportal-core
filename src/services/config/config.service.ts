import {Injectable} from '@angular/core';
import {Http} from '@angular/http';
import {Observable} from 'rxjs/Observable';
import {IOptions} from '../../interfaces/view-options.interface';

@Injectable()
export class ConfigService {

    private uniqueId: number = 0;

    constructor(private http: Http) {
    }

    /**
     * Return portal config in json
     */
    getPortalConfig() {
        return this.http.get('portal/portal-config.json')
            .map((res) => {
                return this.expandOptions(res.json());
            });
    }

    /**
     * Recursively extend each route
     */
    expandOptions(options: any): IOptions {
        for (let route in options.routes) {
            if (options.routes.hasOwnProperty(route)) {
                // Add unique id
                for (let comp of options.routes[route].components) {
                    comp.uniqueId = this.uniqueId++;
                }
                // Extend routes
                if (options.routes[route].extends !== undefined) {
                    options.routes[route] = this.extendRoute(options.routes[route], options.routes);
                }
                // Remove components
                if (options.routes[route].removeComponents !== undefined) {
                    for (let removeCompId of options.routes[route].removeComponents) {
                        options.routes[route].components = this.removeComponent(removeCompId, options.routes[route].components);
                    }
                }
                // Sort components
                for (let comp of options.routes[route].components) {
                    if (comp.beforeComponent !== undefined) {
                        options.routes[route].components =
                            this.insertComponentAt(comp, 'before', comp.beforeComponent, options.routes[route].components);
                    } else if (comp.afterComponent !== undefined) {
                        options.routes[route].components =
                            this.insertComponentAt(comp, 'after', comp.afterComponent, options.routes[route].components);
                    }
                }
            }
        }
        return <IOptions>options;
    }

    extendRoute(curRoute, allRoutes) {
        let parentRoute = allRoutes[curRoute.extends];
        if (parentRoute.extends !== undefined) {
            parentRoute = this.extendRoute(parentRoute, allRoutes);
        }

        // Merge parent route with current

        // Merge attributes
        let mergeAttrs = ['auth', 'view'];
        for (let attrName of mergeAttrs) {
            if (parentRoute[attrName] !== undefined) {
                curRoute[attrName] = parentRoute[attrName];
            }
        }
        // Merge components
        let currComponents = curRoute.components;
        curRoute.components = [];
        let isFoundComponent;
        for (let parentComponent of parentRoute.components) {
            isFoundComponent = false;
            for (let currComponent of currComponents) {
                if (parentComponent.id === currComponent.id) {
                    isFoundComponent = true;
                }
            }
            if (!isFoundComponent) {
                curRoute.components.push(parentComponent);
            }
        }
        for (let comp of currComponents) {
            curRoute.components.push(comp);
        }

        // Set new key - extended - so that we dont expand the already expanded route
        curRoute.extended = curRoute.extends;
        delete curRoute.extends;

        return curRoute;
    }

    removeComponent(removeCompId, components) {
        for (let foundComp of components) {
            if (removeCompId === foundComp.id) {
                let indx = components.indexOf(foundComp);
                if (indx !== -1) {
                    components.splice(indx, 1);
                    break;
                }
            }
        }
        return components;
    }

    insertComponentAt(inComp, where, atCompId, components) {
        for (let foundComp of components) {
            if (atCompId === foundComp.id) {
                let indx = components.indexOf(foundComp);
                if (indx !== -1) {
                    components = this.removeComponent(inComp.id, components);
                    if (where === 'before') {
                        components.splice(indx, 0, inComp);
                        break;
                    } else if (where === 'after') {
                        components.splice(indx + 1, 0, inComp);
                        break;
                    }
                }
            }
        }
        return components;
    }

}
