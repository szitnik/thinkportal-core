import {
  ViewContainerRef, ComponentRef, ComponentFactoryResolver, Injectable, ComponentFactory, ApplicationRef
} from '@angular/core';
import {
  IOptions, IRoute, IComponent, IComponentDictionary,
  IDataView, ILayoutComponent, ILocationRoute
} from '../../interfaces/view-options.interface';
import { IEvent } from '../../interfaces/event-options.interface';
import { Observable } from 'rxjs/Rx';
import { Observer } from 'rxjs/Rx';
import { Title, DomSanitizer } from '@angular/platform-browser';
import { ConfigService } from '../config/config.service';
import { EventService } from '../event/event.service';
import { AuthService } from '../auth/auth.service';
import * as _ from 'lodash';

@Injectable()
export class CoreService {
  public componentRefs: ComponentRef<any>[];
  private loadedFactoriesInstances: any;
  private componentByContainers: any;
  public layoutsRefs: { [id: number]: ComponentRef<any> };
  public containerRef: ViewContainerRef;
  public currentRouteOptions: IRoute;
  public options: IOptions;
  private _observable;
  public view$: Observable<IRoute>;
  private _viewObserver: Observer<IRoute>;
  private _baseUrl: string;
  private importPromises: any[];

  constructor(
    private componentResolver: ComponentFactoryResolver,
    private title: Title,
    private configService: ConfigService,
    private eventService: EventService,
    private authService: AuthService,
    private appRef: ApplicationRef,
    private sanitizer: DomSanitizer
  ) {
    this.componentRefs = [];
    this.layoutsRefs = {};
    this._observable = Observable;
    this.view$ = new Observable(observer => this._viewObserver = observer).share();
    this.componentByContainers = {};
    this.loadedFactoriesInstances = {};

    this.subscribeRenderView();
  }

  loadConfig() {
    return this.configService.getPortalConfig()
      .map((options: IOptions) => {
        this.options = options;
        return this.options;
      });
  }

  bootstrap(): Observable<any> {
    return this.authService.init();
  }

  importModules(promises: any[]) {
    this.importPromises = promises;
  }

  /**
   * Config is loaded and then layout is rendered
   * @param viewContainerRef
   * @param name
   * @returns {any}
   */
  renderView(viewContainerRef: any, location: string): void {
    if (viewContainerRef) {
      this.containerRef = viewContainerRef;
    }

    // Parse location and route options
    let route = this.parseLocation(location, this.options);
    this.currentRouteOptions = this.getRouteOptions(this.options, route.name);

    // Check if authentication is required then continue
    this.authService.checkAuth(this.currentRouteOptions.auth).subscribe(
      () => {
        this.loadAllReference(this.options, this.currentRouteOptions);
      },
      (error) => {
        console.log(' **CORE** Auth error', error);
        this.redirectLoginRoute(this.options);
      });
  }

  subscribeRenderView() {
    this.eventService.subscribe('References.Loaded', (references: IEvent) => {
      this.load(this.containerRef, this.currentRouteOptions, <ILayoutComponent>{
        reference: references.data,
        params: { components: references.data.components, core: this, data: {} }
      });
      this.title.setTitle(references.data.options.title);
      this._viewObserver.next(references.data.options);
    });
  }

  parseBaseUrl(): string {
    if (this._baseUrl === undefined) {
      let baseUrl = (<any>document.getElementsByTagName('base')[0]).href;
      let origin = window.location.origin;
      baseUrl = baseUrl.replace(new RegExp('^' + origin), '');
      baseUrl = baseUrl.replace(new RegExp('^/?'), '');
      this._baseUrl = baseUrl.replace(new RegExp('/?$'), '');
    }
    return this._baseUrl;
  }

  /**
   * URL location is parsed
   * Taking into account Base URL
   *
   * @param location
   * @returns {{name: string, params: Array}}
   */
  parseLocation(location: string, options: IOptions): ILocationRoute {
    location = location.replace(new RegExp('^' + this.parseBaseUrl() + '/'), '');

    let routeArr = location.split('/').slice(1);
    let route: ILocationRoute = {
      name: '',
      params: []
    };
    if (routeArr.length) {
      route.name = routeArr[0];
      routeArr.splice(0, 1);
      if (routeArr.length) {
        route.params = routeArr;
      }
    } else {
      route.name = location;
    }

    // Find route by URL
    for (let optRoute in options.routes) {
      if (options.routes.hasOwnProperty(optRoute)) {
        if (options.routes[optRoute].url === route.name) {
          route.name = optRoute;
          break;
        }
      }
    }

    return route;
  }

  /**
   * Start loading process
   */
  load(viewContainerRef: ViewContainerRef, routeOptions: IRoute, component: ILayoutComponent): void {
    if (this.layoutsRefs[routeOptions.view.type]) {
      if ((<any>this.layoutsRefs[routeOptions.view.type].componentType).name === routeOptions.view.type) {
        viewContainerRef = this.layoutsRefs[routeOptions.view.type];
        // The same layout is used
        if (component && component.params !== undefined) {
          // Set params for component
          for (let key in component.params) {
            if (component.params.hasOwnProperty(key)) {
              this.layoutsRefs[routeOptions.view.type].instance[key] = component.params[key];
            }
          }
        }
        this.eventService.emit('View.Updated', component.params);
      } else {
        for (let layOut in this.layoutsRefs) {
          if (this.layoutsRefs.hasOwnProperty(layOut)) {
            this.layoutsRefs[layOut].destroy();
            delete this.layoutsRefs[layOut];
          }
        }
        this.loadLayout(viewContainerRef, routeOptions, component);
      }
    } else {
      // Remove all
      for (let layOut in this.layoutsRefs) {
        if (this.layoutsRefs.hasOwnProperty(layOut)) {
          this.layoutsRefs[layOut].destroy();
          delete this.layoutsRefs[layOut];
        }
      }
      this.loadLayout(viewContainerRef, routeOptions, component);
    }
  }

  /**
   * Load layout to target view ref
   * @param viewContainerRef
   * @param component
   */
  loadLayout(viewContainerRef: ViewContainerRef, routeOptions: IRoute, component: ILayoutComponent): void {
    // Loop through all and destroy it
    for (let container in this.componentByContainers) {
      for (let componentIndex in this.componentByContainers[container]) {
        this.componentByContainers[container][componentIndex].destroy();
      }
    }
    this.componentByContainers = {};
    // Clear all elements from container
    let componentFactory = this.componentResolver.resolveComponentFactory(component.reference.view);
    let el: ComponentRef<any> = viewContainerRef.createComponent(componentFactory);
    this.layoutsRefs[routeOptions.view.type] = el;
    if (el && component) {
      // Set params for component
      if (component.params !== undefined) {
        for (let key in component.params) {
          if (component.params.hasOwnProperty(key)) {
            el.instance[key] = component.params[key];
          }
        }
      }
    }
    this.eventService.emit('View.Updated', component.params);
  }

  /**
   * Load all dependencies and inject
   * @param options
   * @param name
   * @returns {any}
   */
  loadAllReference(options: IOptions, routeOptions: IRoute): void {
    this._observable.forkJoin(this.importPromises).subscribe(data => {
      let _options = this.pairOptionsWithReferences(data, options, routeOptions);
      let dataViewRef: IDataView = {
        date: new Date().toString(),
        options: routeOptions,
        view: _options.layout.reference,
        name: routeOptions.name,
        components: this.groupComponentsByContainers(_options.routes[routeOptions.name])
      };
      this.eventService.emit('References.Loaded', dataViewRef);
    });

  }

  /**
   * Pair options with route
   * @param data
   * @param options
   * @param routeOptions
   * @returns {IOptions}
   */

  pairOptionsWithReferences(data, options: IOptions, routeOptions: IRoute): IOptions {
    for (let item of data) {
      if (item[routeOptions.view.type]) {
        options.layout.reference = item[routeOptions.view.type];
        options.layout.name = routeOptions.view.type;
        break;
      }
    }

    for (let pkey in routeOptions.components) {
      if (routeOptions.components.hasOwnProperty(pkey)) {
        references:
        for (let key in data) {
          if (data.hasOwnProperty(key)) {
            if (data[key][routeOptions.components[pkey].name]) {
              routeOptions.components[pkey].reference = data[key][routeOptions.components[pkey].name];
              break references;
            }
          }
        }
      }
    }
    return options;
  }

  /**
   * Group components by its container
   * @param route
   * @returns {{}}
   */
  groupComponentsByContainers(route: IRoute): IComponentDictionary {
    let _components: IComponentDictionary = {};
    for (let key in route.components) {
      if (route.components.hasOwnProperty(key)) {
        if (!_components[route.components[key].container]) {
          // Create propery in object
          _components[route.components[key].container] = [];
        }
        _components[route.components[key].container].push(route.components[key]);
      }
    }
    return _components;
  }

  /**
   * Used in all layout containers
   * @param viewContainerRef
   * @param components
   */
  loadComponentsFromReference(viewContainerRef: ViewContainerRef, components: IComponent[], containerName: string): void {
    // Remove not needed components
    if (components !== undefined) {
      if (!this.componentByContainers[containerName]) {
        this.componentByContainers[containerName] = {};
      } else {
        // Need to remove what doesn't belong to current route from current container
        for (let itemContainerIndex in this.componentByContainers[containerName]) {
          let hasValue = false;
          for (let itemNew of components) {
            if (itemContainerIndex === itemNew.uniqueId.toString()) {
              hasValue = true;
            }
          }
          if (!hasValue) {
            this.componentByContainers[containerName][itemContainerIndex].destroy();
            delete this.componentByContainers[containerName][itemContainerIndex];
          }
        }
      }

      // Load new components
      for (let item of components) {
        // Need to check if component name with the same unique_id is already loaded
        if (!_.has(this.componentByContainers[containerName], item.uniqueId.toString())) {
          // Check if instance of component exist in factory
          if (!this.loadedFactoriesInstances[item.name]) {
            let componentFactory: ComponentFactory<any> = this.componentResolver.resolveComponentFactory(<any>item.reference);
            this.loadedFactoriesInstances[componentFactory.componentType.name] = componentFactory;
            this.loadFactoryInstance(viewContainerRef, componentFactory, containerName, item, this.componentByContainers);
          } else {
            // Load from stored factory
            this.loadFactoryInstance(viewContainerRef, this.loadedFactoriesInstances[item.name], containerName, item, this.componentByContainers);
          }
        } else {
          // Component already loaded - leave it
          this.eventService.emit('Component.Updated.' + item.name, item.params);
        }
      }
    }
  }

  /**
   * used to load factory into view container
   */
  loadFactoryInstance(viewContainerRef: ViewContainerRef, factory: ComponentFactory<any>, containerName: string, item: IComponent, componentByContainers: any) {
    if (viewContainerRef) {
      let el: ComponentRef<any> = viewContainerRef.createComponent(factory);
      componentByContainers[containerName][item.uniqueId.toString()] = el;

      if (el && item.params) {
        // Extends current component instance with all params provided to current component through layout component
        el.instance['params'] = item.params;
        el.instance['parentViewRef'] = viewContainerRef;
        el.instance['componentId'] = item.uniqueId.toString();

        if (el && item.modifiers) {
          // Set class for component
          if (item.modifiers.class !== undefined) {
            el.location.nativeElement.setAttribute('class', item.modifiers.class);
          }
          // Set style for component
          if (item.modifiers.style !== undefined) {
            el.location.nativeElement.setAttribute('style', item.modifiers.style);
          }
        }
      }
    }
  }

  /**
   * Return route options
   */
  getRouteOptions(options: IOptions, name: string): IRoute {
    let _name = (options.routes[name]) ? name : options.defaultRoute;
    let routeOptions: IRoute = options.routes[_name];
    routeOptions.name = _name;
    return routeOptions;
  }

  /**
   * Redirect to default route
   * @param options:IOptions
   */
  redirectDefaultRoute(options: IOptions): void {
    this.go(options.defaultRoute);
  }

  /**
   * Redirect to login route
   * @param options:IOptions
   */
  redirectLoginRoute(options: IOptions): void {
    this.go(options.loginRoute);
  }

  /**
   *
   * @param route
   */
  go(route: string): void {
    console.log(' **CORE** Go to route: ', route);
    window.history.pushState(route, route, route);
    this.renderView(null, route);

  }

  getLayoutModifier(view: string, container: string, modifier: string): string {
    if (!_.isEmpty(this.options.layout.views[view].containers[container].modifiers[modifier])) {
      return <string>this.sanitizer.bypassSecurityTrustStyle(this.options.layout.views[view].containers[container].modifiers[modifier]);
    } else {
      return '';
    }
  }

  setOptions(options: IOptions) {
    this.options = options;
  }
}
