import {Injectable} from '@angular/core';
import {Store} from '@ngrx/store';
import {AppState} from '../../interfaces/app-state.interface';

@Injectable()
export class StateService {

    constructor(private store: Store<AppState>) {}

    public getValue(key: string): any {
        let appState: AppState;
        this.store.take(1).subscribe(s => appState = s);
        return appState[key];
    }

    public getStore(): Store<AppState> {
        return this.store;
    }

}
