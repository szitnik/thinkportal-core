import {Injectable} from "@angular/core";
import {Observable} from "rxjs/Rx";
import {IAuth, IAuthService} from "../../interfaces/auth-options.interface";
import {EventService} from "../event/event.service";
import {IAuthRouteConfig} from "../../interfaces/view-options.interface";

@Injectable()
export class AuthService {

    private authKey: string = 'Auth';
    private authData: any;
    private checkAuthCallback: Function;
    private initAuthCallback: Function;
    private logoutAuthCallback: Function;

    constructor(private eventService: EventService) {
    }

    init(): any {
        if (this.initAuthCallback !== undefined) {
            return this.initAuthCallback();
        } else {
            return Observable.of(null);
        }
    }

    login(user: any, service: IAuthService) {
        let authData = <IAuth>{"user": user, "service": service};
        this.saveAuth(authData);
        this.setLoggedIn(authData);
    }

    logout() {
        let authData = this.getAuth();
        this.removeAuth();
        this.setLoggedOut(authData);
    }

    completeLogout() {
        let authData = this.getAuth();
        this.removeAuth();
        if (this.logoutAuthCallback !== undefined) {
            this.logoutAuthCallback();
        }
        this.setLoggedOut(authData);
    }

    setLoggedIn(authData) {
        this.authData = authData;
        this.eventService.emit('Auth.LoggedIn', authData);
    }

    setLoggedOut(authData) {
        this.authData = undefined;
        this.eventService.emit('Auth.LoggedOut', authData);
    }

    saveAuth(authData: IAuth): void {
        sessionStorage.setItem(this.authKey, JSON.stringify(authData));
    }

    removeAuth(): void {
        sessionStorage.removeItem(this.authKey);
    }

    getAuth(): IAuth {
        let data = <IAuth>JSON.parse(sessionStorage.getItem(this.authKey));
        if (data && data.service && data.service.expiresOn && data.service.expiresOn <= (new Date().getTime() + 5000)) {
            // remove token if expired
            this.removeAuth();
            this.setLoggedOut(data);
            return null;
        } else {
            return data;
        }
    }

    checkAuth(authConfig: IAuthRouteConfig): Observable<any> {
        return Observable.create((observer) => {
            let authData: IAuth = this.getAuth();
            if (authConfig.required) {
                if (authData && authData.user) {
                    observer.next(authData.user);
                } else {
                    observer.error(new Error('Need to login first'));
                }
            } else {
                if (authData && authData.user) {
                    observer.next(authData.user);
                } else {
                    observer.next(null);
                }
            }
            observer.complete();
        });
    }

    isLoggedIn(): boolean {
        let authData: IAuth = this.getAuth();
        if (authData && authData.user) {
            return true;
        }
        return false;
    }

    registerInterceptors(interceptors: any) {
        if (interceptors.init !== undefined) {
            this.initAuthCallback = <Function>interceptors.init;
        }
        if (interceptors.checkAuth !== undefined) {
            this.checkAuthCallback = <Function>interceptors.checkAuth;
        }
        if (interceptors.logout !== undefined) {
            this.logoutAuthCallback = <Function>interceptors.logout;
        }
    }

}
