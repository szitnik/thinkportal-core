/**
 * ThinkComponent decorator should be used by every component which can be defined in portal-config.
 * Almost everything works but the properties which get injected. Those are not available in overridden constructor.
 */

import {ReflectiveInjector} from '@angular/core';
import {EventService} from "../services/event/event.service";

export function ThinkComponent(annotation?: any) {
    return function (target: Function) {
        var original = target;

        // The new constructor
        var f: any = function (...args) {
            // Get a reference to EventService singleton
            var injector = ReflectiveInjector.resolveAndCreate([EventService]);
            var eventServ = injector.get(EventService);
            // Use onEvent metadata to setup appropriate subscriptions
            eventServ.subscribe('Auth.LoggedIn', original.prototype.onAuthLoggedIn);

            return original.apply(this, args);
        }

        // Copy prototype so intanceof operator still works
        f.prototype = original.prototype;

        // Copy all metadata - should use getMetadataKeys for copying all used metadata
        Reflect.defineMetadata('annotations', Reflect.getMetadata('annotations', target), f);
        Reflect.defineMetadata('propMetadata', Reflect.getMetadata('propMetadata', target), f);

        // Return new constructor (will override original)
        return f;
    }
}