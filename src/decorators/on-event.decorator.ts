/**
 * Decorator for event callback functions
 * The idea is very simple. Decorator should just append custom metadata to the class.
 * This way the constructor knows which methods should be treated as callbacks.
 *
 * @OnEvent('Auth.LoggedIn')
 * onAuthLoggedIn(user) {
 *   console.log('Event called', user);
 * }
 */

export function OnEvent(eventName: string) {
    return function (target: any, key: string, value: any) {
        Reflect.defineMetadata('onEvent', eventName, target);
    };
}