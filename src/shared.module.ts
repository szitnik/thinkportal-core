import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {IconComponent} from './components/icon.component';
import {ModalService} from './services/modal/modal.service';

@NgModule({
    imports: [CommonModule],
    declarations: [IconComponent],
    providers: [ModalService],
    exports: [CommonModule, IconComponent]
})
export class SharedModule { }