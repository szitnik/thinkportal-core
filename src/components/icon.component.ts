import {Component, Input} from '@angular/core';

@Component({
    selector: 'icon',
    template: '<i class="material-icons">{{iconName}}</i>',
    styleUrls: ['icon.component.scss']
})
export class IconComponent {
    public iconName: string = '';

    @Input() set name(name) {
        this.iconName = name;
    }

}