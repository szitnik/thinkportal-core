import {NgModule, Optional, SkipSelf} from '@angular/core';
import {CommonModule} from '@angular/common';
import {CoreService} from "./services/core/core.service";
import {AuthService} from './services/auth/auth.service';
import {ConfigService} from './services/config/config.service';
import {EventService} from './services/event/event.service';
import {StateService} from './services/state/state.service';

@NgModule({
  imports: [CommonModule],
  providers: [
    { provide: CoreService, useClass: CoreService },
     { provide: ConfigService, useClass: ConfigService },
     { provide: EventService, useClass: EventService },
     { provide: StateService, useClass: StateService },
     { provide: AuthService, useClass: AuthService, deps:[EventService] }
  ],
  exports:[]
})
export class ThinkPortalCoreModule {
    constructor (@Optional() @SkipSelf() parentModule: ThinkPortalCoreModule) {
        if (parentModule) {
            throw new Error('ThinkPortalCoreModule is already loaded. Import it in the AppModule only.');
        }
    }
    static reducers() {
        return {};
    }
}